let townDirections = `Type town.go('Place') to go to any of the following places : \n\n\n` + 
`Inn : Restore Hp/Mp  -  town.go('inn')\n\nMerchant : Buy new weapons and armors  -  town.go('merchant')\n\n` + 
`Spell Shop : Buy new spells  -  town.go('spells')\n\nTrainer : Can train your stats  -  town.go('trainer')\n\n` +
`Adventurers Guild : You can get quests to do in the dungeons  -  town.go('guild')\n\n` +
`Dungeons : You can access dungeons from here  -  town.go('dungeons')\n\n\nType game.help() anytime to get help on how to play.\n`,
	notAllowed = `\nYou're not allowed to do that !\n`;

let hero;

console.log('Welcome to Console Game !\n\n\n If you need help just type game.help().\n\n If you want to start the game, ' + 
	'type game.start()');


