let items = {

	stick: {
		name: 'Stick',
		atk: 1,
		type: 'weapon',
		weaponType: 'stick',
		roleAllowed: 1,
		possessed: 0,
		code: 'stick',
		quality: 0,
		buyable: false,
		hit: 85
	},

	woodensword: {
		name: 'Wooden Sword',
		atk: 3,
		price: 15,
		type: 'weapon',
		weaponType: 'sword',
		roleAllowed: 3,
		possessed: 0,
		code: 'woodensword',
		quality: 1,
		buyable: true,
		hit: 90
	},

	woodenstaff: {
		name: 'Wooden Staff',
		int: 1,
		price: 15,
		type: 'weapon',
		weaponType: 'staff',
		roleAllowed: 5,
		possessed: 0,
		code: 'woodenstaff',
		quality: 1,
		buyable: true,
		hit: 85
	},

	stonemace: {
		name: 'Stone Mace',
		atk: 2,
		int: 0,
		price: 15,
		type: 'weapon',
		weaponType: 'mace',
		roleAllowed: 4,
		possessed: 0,
		code: 'stonemace',
		quality: 1,
		buyable: true,
		hit: 85
	},



	leathershield: {
		name: 'Leather Shield',
		def: 3,
		price: 15,
		type: 'offHand',
		offHandType: 'shield',
		roleAllowed: 2,
		possessed: 0,
		code: 'leathershield',
		quality: 1,
		buyable: true
	},

	scrapedbook: {
		name: 'Scraped Book',
		int: 1,
		price: 15,
		type: 'offHand',
		offHandType: 'book',
		roleAllowed: 6,
		possessed: 0,
		code: 'scrapedbook',
		quality: 1,
		buyable: true
	},



	clotharmor: {
		name: 'Cloth Armor',
		def: 2,
		price: 10,
		type: 'armor',
		armorType: 'chest',
		roleAllowed: 1,
		possessed: 0,
		code: 'clotharmor',
		quality: 1,
		buyable: true
	},

	leatherarmor: {
		name: 'Leather Armor',
		def: 3,
		price: 15,
		type: 'armor',
		armorType: 'chest',
		roleAllowed: 2,
		possessed: 0,
		code: 'leatherarmor',
		quality: 1,
		buyable: true
	},

	soldierarmor: {
		name: 'Soldier Armor',
		def: 4,
		price: 18,
		type: 'armor',
		armorType: 'chest',
		roleAllowed: 3,
		possessed: 0,
		code: 'soldierarmor',
		quality: 1,
		buyable: true
	},



	clothboots: {
		name: 'Cloth Boots',
		def: 1,
		spe: 0,
		price: 10,
		type: 'armor',
		armorType: 'foot',
		roleAllowed: 1,
		possessed: 0,
		code: 'clothboots',
		quality: 1,
		buyable: true
	},

	leatherboots: {
		name: 'Leather Boots',
		def: 2,
		spe: 0,
		price: 15,
		type: 'armor',
		armorType: 'foot',
		roleAllowed: 2,
		possessed: 0,
		code: 'leatherboots',
		quality: 1,
		buyable: true
	},

	soldierboots: {
		name: 'Soldier Boots',
		def: 3,
		spe: 0,
		price: 17,
		type: 'armor',
		armorType: 'foot',
		roleAllowed: 3,
		possessed: 0,
		code: 'soldierboots',
		quality: 1,
		buyable: true
	},



	clothhelmet: {
		name: 'Cloth Helmet',
		def: 1,
		price: 10,
		type: 'armor',
		armorType: 'head',
		roleAllowed: 1,
		possessed: 0,
		code: 'clothhelmet',
		quality: 1,
		buyable: true
	},

	leatherhelmet: {
		name: 'Leather Helmet',
		def: 2,
		price: 15,
		type: 'armor',
		armorType: 'head',
		roleAllowed: 2,
		possessed: 0,
		code: 'leatherhelmet',
		quality: 1,
		buyable: true
	},

	soldierhelmet: {
		name: 'Soldier Helmet',
		def: 3,
		price: 20,
		type: 'armor',
		armorType: 'head',
		roleAllowed: 3,
		possessed: 0,
		code: 'soldierhelmet',
		quality: 1,
		buyable: true
	},



	smallhealthpotion: {
		name: 'Small Health Potion',
		type: 'item',
		itemType: 'potion',
		effect: 'lifeup',
		power: 100,
		price: 20,
		roleAllowed: 1,
		description: `Restore 100 Hp.`,
		possessed: 0,
		code: 'smallhealthpotion',
		quality: 1,
		buyable: true
	},



	glue: {
		name: 'Glue',
		type: 'loot',
		price: 10,
		possessed: 0,
		code: 'glue',
		rate: [0, 50],
		buyable: false
	},

	batwings: {
		name: 'Bat Wings',
		type: 'loot',
		price: 12,
		possessed: 0,
		code: 'batwings',
		rate: [0, 50],
		buyable: false
	}
}