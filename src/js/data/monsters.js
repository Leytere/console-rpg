let monstersSkills = {

	ultrasound: {
		name: 'Ultrasound',
		code: 'ultrasound',
		mpCost: 3,
		target: 'player',
		type: 'debuff',
		hit: 70
	},

	jellypulse: {
		name: 'Jelly Pulse',
		code: 'jellypulse',
		mpCost: 4,
		power: 30,
		target: 'player',
		type: 'dot',
		dotType: 'direct',
		hit: 70
	},

	jellypara: {
		name: 'Jelly Paralysis',
		code: 'jellypara',
		mpCost: 5,
		target: 'player',
		type: 'debuff',
		debuffType: 'para',
		hit: 60
	},

	jellheal: {
		name: 'JellHeal',
		code: 'jellheal',
		mpCost: 4,
		power: 50,
		target: 'monster',
		type: 'heal',
		healType: 'weak',
		hit: 100
	},

	defend: {
		name: 'Defend',
		code: 'defend',
		type: 'defend'
	}
};

let monsters =  {
	
	slime: {
		name: 'Slime',
		code: 'slime',
		hp: 10,
		mp: 0,
		maxHp: 10,
		maxMp: 0,
		atk: 10,
		def: 3,
		spe: 5,
		int: 0,
		gold: 3,
		xp: 3,
		hit: 80,
		loots: {
			glue: items.glue
		},
		skills: {
			defend: monstersSkills.defend
		},
		skillChances: [[30, 'defend']]
	},

	bat: {
		name: 'Bat',
		code: 'bat',
		hp: 10,
		mp: 0,
		maxHp: 10,
		maxMp: 3,
		atk: 8,
		def: 2,
		spe: 4,
		int: 0,
		gold: 3,
		xp: 2,
		hit: 90,
		skills: {
			ultrasound: monstersSkills.ultrasound,
			defend: monstersSkills.defend
		},
		skillChances: [[21, 'ultrasound'], [51, 'defend']],
		loots: {
			batwings: items.batwings
		}
	}
};

