let clericSpells = {
	smallheal: {
		name: 'Small Heal',
		mpCost: 8,
		power: 40,
		hit: 100,
		target: 'self',
		type: 'heal',
		lvl: 1,
		price: 30,
		description: `Heal yourself for a small amount.`,
		role: 'Cleric',
		code: 'smallheal'
	}
}

let mageSpells = {
	bolt: {
		name: 'Bolt',
		mpCost: 10,
		power: 40,
		hit: 90,
		target: 'enemy',
		type: 'directAtk',
		lvl: 1,
		price: 30,
		description: `Attack one enemy with a weak bolt.`,
		role: 'Mage',
		code: 'bolt'
	},

	poison: {
		name: 'Poison',
		mpCost: 8,
		power: 20,
		hit: 85,
		target: 'enemy',
		type: 'dot',
		dotType: 'delayed',
		lvl: 1,
		price: 30,
		description: `Inflict weak damages over time on one enemy.`,
		role: 'Mage',
		code: 'poison'
	}
}