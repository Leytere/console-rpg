let questList = {

	forest: {

		glue: {
			name: 'Glue Retrieval',
			description: 'Fetch 10 Glues from the Slimes in the Forest.',
			reward: 50,
			xp: 50,
			finished: false,
			completion: false,
			taken: false,
			objective: ['Glue', 10, 0],
			place: 'forest'
		},

		bat: {
			name: 'Bat Killing',
			description: 'Kill 15 Bats.',
			reward: 50,
			xp: 50,
			finished: false,
			completion: false,
			taken: false,
			objective: ['Bat', 15, 0],
			place: 'forest'
		}
	}
}