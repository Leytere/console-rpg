class Forest {
	constructor() {
		this.roomN = _.random(7, 11),
		this.heroPos = 0,
		this.eventChoice = 0,
		this.on = true;
		this.events = {
			battle1: {
				type: 'battle',
				monsters: [monsters.slime],
				chances: [20]
			},

			battle2: {
				type: 'battle',
				monsters: [monsters.bat],
				chances: [40]
			},

			battle3: {
				type: 'battle',
				monsters: [monsters.slime, monsters.bat],
				chances: [60]
			},

			restoration: {
				type: 'restoration',
				restoType: 'weakHp',
				chances: [66]
			},

			battle4: {
				type: 'battle',
				monsters: [monsters.slime, monsters.slime, monsters.bat],
				chances: [80]
			},

			trap: {
				type: 'trap',
				trapType: 'weakHp',
				chances: [90]
			},

			mpTrap: {
				type: 'trap',
				trapType: 'weakMp',
				chances: [100]
			}
		}
	}

	next() {
		if (!hero.onCombat) {
			if (this.heroPos < this.roomN) {
				this.heroPos++;
				this.eventChoice = _.random(1, 100);
				let loopFirst = true;
				let p;
				for (let o in this.events) {
					if (this.eventChoice <= this.events[o].chances && loopFirst) {

						this.finalChoice = this.events[o];
					} else if (this.eventChoice <= this.events[o].chances &&
						this.eventChoice > this.events[p].chances) {

						this.finalChoice = this.events[o];
					}
					p = o;
				}
				game.compFunc = true;
				game.eventLaunch(this.finalChoice);
			}
		} else {
			return `\nYou can't do that while you fight !\n`;
		}
	}

	leave() {
		if (!hero.onCombat) {
			town.position = 1;
			hero.dungeon = null;
			console.log(`\nYou leave the Forest.\n\nYou're back in town.\n\n\n` + townDirections);
			delete this;
		} else {
			return `\nYou can't do that while you fight !\n`;
		}
	}

	delete() {
		delete this;
	}
}