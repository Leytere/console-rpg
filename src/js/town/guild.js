let guild = {

	compFunc: false,
	dungeonChoice: null,
	questChoice: null,

	dungeon(choice) {
		if (town.guildPos === 1) {
			choice = choice[0].toLowerCase() + choice.substr(1);
			switch (choice) {
				case 'forest':
				town.guildPos++;
				this.dungeonChoice = 'forest';
				console.log('Forest Quests \n\n\n')
				for (let o in questList.forest) {
					if (questList.forest[o].finished || questList.forest[o].taken) {
						continue;
					} else {
						console.log(`${questList.forest[o].name}  -  guild.quest('${o}')\n\n`);
					}
				}
				return `\nType the command next to the quest name to get info and take the quest.\n`;
				break;

				default:
				return `\nThis is not an allowed choice !\n`;
				break;
			}
		} else {
			return notAllowed;
		}
	},

	quest(choice) {
		if (town.guildPos === 2) {
			switch (this.dungeonChoice) {
				case 'forest':
				for (let o in questList.forest) {
					if (o == choice) {
						if (questList.forest[o].finished || questList.forest[o].taken) {
							return `\nThis is not an allowed choice !\n`;
						} else {
							town.guildPos++;
							this.questChoice = questList.forest[o];
							questList.forest[o].taken = true;
							return `\n${this.questChoice.name}\n\n${this.questChoice.description}\n\n` +
							`Reward : ${this.questChoice.reward} Gold\n\nType guild.accept() to accept quest.\n`;
						}
					}
				}
				return `\nThis is not an allowed choice !\n`;
				break;
			}
		} else {
			return notAllowed;
		}
	},

	accept() {
		if (town.guildPos === 3) {
			hero.questLog[this.questChoice.name] = Object.create(this.questChoice);
			town.position = 1;
			town.guildPos = 0;
			return `\nYou accepted the quest.\n\nIt's been put in your Quest Log.\n\nYou're back in town.\n\n\n` +
			townDirections;
		} else {
			return notAllowed;
		}
	},

	complete(choice = 'no') {
		if (town.guildPos === 1) {
			let noComplete = true;
			let falseComplete = true;
			for (let o in hero.questLog) {
				console.log(o);
				if (hero.questLog[o].completion) {
					noComplete = false;
					console.log(`\nYou can complete ${hero.questLog[o].name} by typing guild.complete('${o}')\n`);
				}
			}
			if (noComplete) {
				return `\nYou don't have any quest to complete.\n`;
			}
			if (choice !== 'no') {
				for (let o in hero.questLog) {
					if (o == choice) {
						if (hero.questLog[o].completion) {
							falseComplete = false;
							hero.gold += hero.questLog[o].reward;
							hero.xp += hero.questLog[o].xp;
							console.log(`\nYou gain ${hero.questLog[o].reward} Gold.\n\nYou gain ${hero.questLog[o].xp} Xp\n\n`);
							if (hero.questLog[o].place === 'forest') {
								for (let p in questList.forest) {
									if (hero.questLog[o].name === questList.forest[p].name) {
										questList.forest[p].finished = true;
									}
								}
							}
							delete hero.questLog[o];
						}
					}
				}
				if (!falseComplete) {
					return `\nYou completed a quest.\n`;
				} else {
					return `\nIt's not an allowed choice !\n`;
				}
			}
			return `\nEnd\n`;
		} else {
			return notAllowed;
		}
	},

	leave() {
		if (town.guildPos !== 0) {
			town.position++;
			town.guildPos = 0;
			return `\nYou're back in town.\n\n\n` + townDirections;
		} else {
			return notAllowed;
		}
	}
};