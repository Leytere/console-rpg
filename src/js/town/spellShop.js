let spells = {
	type(role) {
		if (town.spellsPos === 1) {
			role = role[0].toUpperCase() + role.substr(1);
			switch (role) {
				case 'Cleric':
				this.classChoice = 'Cleric';
				town.spellsPos = 2;
				return `\nChoose the Spell Level between 1 and 5.\n\nType spells.level(number) to choose.\n`;
				break;

				case 'Mage':
				this.classChoice = 'Mage';
				town.spellsPos = 2;
				return `\nChoose the Spell Level between 1 and 5.\n\nType spells.level(number) to choose.\n`;
				break;

				default:
				return notAllowed;
				break;
			}
		} else {
			return notAllowed;
		}
	},

	level(num) {
		if (town.spellsPos === 2) {
			if (typeof num === 'number' && num > 0 && num < 6) {
				this.levelChoice = num;
				this.compFunc = true;
				return this.displaySpells();
			} else {
				return notAllowed;
			}
		} else {
			return notAllowed;
		}
	},

	displaySpells() {
		if (this.compFunc) {
			this.compFunc = false;
			town.spellsPos = 1;
			console.log(`\nType the command to see the info and buy the spell.\n\nI have : \n\n`);
			switch (this.classChoice) {
				case 'Cleric':
				for (let o in clericSpells) {
					if (clericSpells[o].lvl == this.levelChoice) {
						console.log(`\n${clericSpells[o].name}  -  spells.info('${o}')\n`);
					}
				}
				return `\nEnd\n`;
				break;

				case 'Mage':
				for (let o in mageSpells) {
					if (mageSpells[o].lvl == this.levelChoice) {
						console.log(`\n${mageSpells[o].name}  -  spells.info('${o}')\n`);
					}
				}
				return `\nEnd\n`;
				break;
			}
		} else {
			return notAllowed;
		}
	},

	info(choice) {
		if (town.spellsPos > 0) {
			let wrongChoice = true;
			if (this.classChoice === 'Cleric') {
				for (let o in clericSpells) {
					if (o == choice) {
						wrongChoice = false;
						this.choice = clericSpells[o];
					}
				}
			} else {
				for (let o in mageSpells) {
					if (o == choice) {
						wrongChoice = false;
						this.choice = mageSpells[o];
					}
				}
			}
			if (wrongChoice) {
				return notAllowed;
			}
			return `${this.choice.name}\n\nMP Cost : ${this.choice.mpCost}\nPrice : ${this.choice.price} Gold\n\n` +
			`${this.choice.description}\n\nYou have ${hero.gold} Gold.\n\n` +
			`If you want to buy it you need to type spells.buy()`;
		} else {
			return notAllowed;
		}
	},

	buy(choice) {
		if (town.spellsPos > 0) {
			let wrongChoice = true;
			for (let o in mageSpells) {
				if (o == choice) {
					this.choice = mageSpells[o];
					wrongChoice = false;
				}
			}
			for (let o in clericSpells) {
				if (o == choice) {
					this.choice = clericSpells[o];
					wrongChoice = false;
				}
			}
			if (wrongChoice) {
				return `\nIt's not an allowed choice !\n`;
			}
			for (let o in hero.spells) {
				if (this.choice.name === hero.spells[o].name) {
					wrongChoice = true;
				}
			}
			if (wrongChoice) {
				return `\nYou already have that spell !\n`;
			}
 			if (this.choice.role === hero.role) {
				if (hero.gold >= this.choice.price) {
					hero.gold -= this.choice.price;
					town.position = 1;
					town.spellsPos = 0;
					hero.spells[this.choice.code] = Object.create(this.choice);
					return `\nThanks for your purchase !\n\nSee you again.\n\nYou're back in town.\n\n\n` +
					townDirections;
				} else {
					town.position = 1;
					town.spellsPos = 0;
					return `\nYou don't have enough gold to buy the spell !\n\n` +
					`You've been kicked out the shop and you're back in town.\n\n\n` +
					townDirections;
				}
			} else {
				town.position = 1;
				town.spellsPos = 0;
				return `\nYou're not allowed to buy spells for this class !\n\n` +
				`You've been kicked out the shop and you're back in town.\n\n\n` +
				townDirections;
			}
		} else {
			return notAllowed;
		}
	},

	leave() {
		if (town.spellsPos !== 0) {
			town.spellsPos = 0;
			town.position = 1;
			return `\nYou're back in town.\n\n\n` + townDirections;
		} else {
			return notAllowed;
		}
	}
}