let trainer = {

	compFunc: false,
	trainPrice: 50,

	stat(choice) {
		if (town.trainerPos === 1) {
			switch (choice) {
				case 'attack':
				town.trainerPos++;
				this.statChoice = 'Attack';
				return `\nThis will cost you ${this.trainPrice}.\n\n` +
				`You have ${hero.gold} Gold.\n\nIf you want to train this stat ` +
				`you must type trainer.train().\n`;
				break;

				case 'intelligence':
				town.trainerPos++;
				this.statChoice = 'Intelligence';
				return `\nThis will cost you ${this.trainPrice}.\n\n` +
				`You have ${hero.gold} Gold.\n\nIf you want to train this stat ` +
				`you must type trainer.train().\n`;
				break;

				case 'speed':
				town.trainerPos++;
				this.statChoice = 'Speed';
				return `\nThis will cost you ${this.trainPrice}.\n\n` +
				`You have ${hero.gold} Gold.\n\nIf you want to train this stat ` +
				`you must type trainer.train().\n`;
				break;

				case 'defense':
				town.trainerPos++;
				this.statChoice = 'Defense';
				return `\nThis will cost you ${this.trainPrice}.\n\n` +
				`You have ${hero.gold} Gold.\n\nIf you want to train this stat ` +
				`you must type trainer.train().\n`;
				break;

				default:
				return `\nThis is not an allowed choice !\n`;
				break;
			}
		} else {
			return notAllowed;
		}
	},

	train() {
		if (town.trainerPos === 2) {
			if (hero.gold >= this.trainPrice) {
				hero.gold -= this.trainPrice;
				this.trainPrice = Math.floor(this.trainPrice * 1.3);
				town.position = 1;
				town.trainerPos = 0;
				switch (this.statChoice) {
					case 'Attack':
					hero.baseAtk++;
					break;

					case 'Defense':
					hero.baseDef++;
					break;

					case 'Speed':
					hero.baseSpe++;
					break;

					case 'Intelligence':
					hero.baseInt++;
					break;
				}
				hero.updateStats();
				return `\nYour ${this.statChoice} is increased by 1 !\n\n` + 
				`Thanks for staying here ! Come back anytime !\n\nYou're now in town.\n\n\n` + townDirections;
			} else {
				town.position = 1;
				town.trainerPos = 0;
				return `\nYou don't have enough gold !\n\nYou've been kicked out the Training Grounds.` +
				` You're now in town.\n\n\n` + townDirections;
			}
		} else {
			return notAllowed;
		}
	},

	leave() {
		if (town.trainerPos !== 0) {
			town.position = 1;
			town.trainerPos = 0;
			return `\nYou leave the Training Grounds, you're now in town.\n\n\n` +
			townDirections;
		} else {
			return notAllowed;
		}
	}
}