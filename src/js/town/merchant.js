let merchant = {
	choice(type) {
		if (town.merchantPos == 1) {
			type = type[0].toUpperCase() + type.substr(1);
			if (type === 'Weapons' || type === 'OffHands' || type === 'Helmets' || type === 'Chests' || type === 'Boots' || type === 'Items') {
				this.typeChoice = type;
				town.merchantPos++;
				return `\nWhat rarity do you want, i have 5 levels of quality.\n\nType merchant.quality('Number') to choose.\n`;
			} else {
				return notAllowed;
			}
		} else {
			return notAllowed;
		}
	},

	quality(num) {
		if (town.merchantPos === 2) {
			if (typeof num === 'number' && num > 0 && num < 6) {
				this.qualityChoice = num;
				this.compFunc = true;
				return this.displayChoice();
			} else {
				return notAllowed;
			}
		} else {
			return notAllowed;
		}
	},

	leave() {
		if (town.merchantPos !== 0) {
			town.merchantPos = 0;
			town.position = 1;
			return `\nYou're back in town.\n\n\n` + townDirections;
		} else {
			return notAllowed;
		}
	},

	displayChoice() {
		if (this.compFunc) {
			town.merchantPos = 1;
			this.compFunc = false;
			console.log(`\nType the command to see informations on the item and buy it.\n`);
			switch (this.typeChoice) {
				case 'Weapons':
				for (let o in items) {
					if (items[o].type === 'weapon' && items[o].quality === this.qualityChoice) {
						console.log(`\n${items[o].name}  -  merchant.info('${o}')\n`);
					}
				}
				return `\nEnd\n`;
				break;

				case 'OffHands':
				for (let o in items) {
					if (items[o].type === 'offHand' && items[o].quality === this.qualityChoice) {
						console.log(`\n${items[o].name}  -  merchant.info('${o}')\n`);
					}
				}
				return `\nEnd\n`;
				break;

				case 'Helmets':
				for (let o in items) {
					if (items[o].type === 'armor' && items[o].armorType === 'head' && items[o].quality === this.qualityChoice) {
						console.log(`\n${items[o].name}  -  merchant.info('${o}')\n`);
					}
				}
				return `\nEnd\n`;
				break;

				case 'Chests':
				for (let o in items) {
					if (items[o].type === 'armor' && items[o].armorType === 'chest' && items[o].quality === this.qualityChoice) {
						console.log(`\n${items[o].name}  -  merchant.info('${o}')\n`);
					}
				}
				return `\nEnd\n`;
				break;

				case 'Boots':
				for (let o in items) {
					if (items[o].type === 'armor' && items[o].armorType === 'foot' && items[o].quality === this.qualityChoice) {
						console.log(`\n${items[o].name}  -  merchant.info('${o}')\n`);
					}
				}
				return `\nEnd\n`;
				break;

				case 'Items':
				for (let o in items) {
					if (items[o].type === 'item' && items[o].quality === this.qualityChoice) {
						console.log(`\n${items[o].name}  -  merchant.info('${o}')\n`);
					}
				}
				return `\nEnd\n`;
				break;
			}
		} else {
			return notAllowed;
		}
	},

	info(choice) {
		if (town.merchantPos > 0) {
			let classAllowed;
			let wrongChoice = true;
			for (let o in items) {
				if (o == choice) {
					wrongChoice = false;
					this.itemChoice = items[o];
					if (this.itemChoice.roleAllowed === 1) {
						classAllowed = `Warrior, Cleric, Mage`;
					} else if (this.itemChoice.roleAllowed === 2) {
						classAllowed = `Warrior, Cleric`;
					} else if (this.itemChoice.roleAllowed === 3) {
						classAllowed = 'Warrior';
					} else if (this.itemChoice.roleAllowed === 4) {
						classAllowed = 'Cleric';
					} else if (this.itemChoice.roleAllowed === 5) {
						classAllowed = 'Mage';
					} else if (this.itemChoice.roleAllowed === 6) {
						classAllowed = 'Cleric, Mage';
					}
				}
			}
			if (wrongChoice) {
				return notAllowed;
			}
			for (let o in items) {
				if (o == choice) {
					if (this.itemChoice.type === 'weapon') {

						if (this.itemChoice.weaponType === 'sword') {
							return `${this.itemChoice.name}\n\nAttack : ${this.itemChoice.atk}\nPrice : ${this.itemChoice.price} Gold\n` +
							`Class Allowed : ${classAllowed}\n\n` + 
							`If you want to buy it type merchant.buy('${this.itemChoice.code}')`;
						} else if (this.itemChoice.weaponType === 'staff') {
							return `${this.itemChoice.name}\n\nIntelligence : ${this.itemChoice.int}\nPrice : ${this.itemChoice.price} Gold\n` +
							`Class Allowed : ${classAllowed}\n\n` +
							`If you want to buy it type merchant.buy('${this.itemChoice.code}')`;
						} else if (this.itemChoice.weaponType === 'mace') {
							return `${this.itemChoice.name}\n\nAttack : ${this.itemChoice.atk}\nIntelligence : ${this.itemChoice.int}\nPrice : ${this.itemChoice.price} Gold\n` +
							`Class Allowed : ${classAllowed}\n\n` +
							`If you want to buy it type merchant.buy('${this.itemChoice.code}')`;
						}

					} else if (this.itemChoice.type === 'offHand') {

						if (this.itemChoice.offHandType === 'shield') {
							return `${this.itemChoice.name}\n\nDefense : ${this.itemChoice.def}\nPrice : ${this.itemChoice.price} Gold\n` +
							`Class Allowed : ${classAllowed}\n\nIf you want to buy it type merchant.buy('${this.itemChoice.code}')`;
						} else {
							return `${this.itemChoice.name}\nIntelligence : ${this.itemChoice.int}\nPrice : ${this.itemChoice.price} Gold\n` +
							`Class Allowed : ${classAllowed}\n\nIf you want to buy it type merchant.buy('${this.itemChoice.code}')`;
						}

					} else if (this.itemChoice.type === 'armor') {

						if (this.itemChoice.armorType === 'chest' || this.itemChoice.armorType === 'head') {
							return `${this.itemChoice.name}\n\nDefense : ${this.itemChoice.def}\nPrice : ${this.itemChoice.price} Gold\n` +
							`Class Allowed : ${classAllowed}\n\nIf you want to buy it type merchant.buy('${this.itemChoice.code}')`;
						} else {
							return `${this.itemChoice.name}\n\nDefense : ${this.itemChoice.def}\nSpeed : ${this.itemChoice.spe}\nPrice : ${this.itemChoice.price} Gold\n` +
							`Class Allowed : ${classAllowed}\n\nIf you want to buy it type merchant.buy('${this.itemChoice.code}')`;
						}

					} else if (this.itemChoice.type === 'item') {

						if (this.itemChoice.itemType === 'potion') {
							return `${this.itemChoice.name}\n\n${this.itemChoice.description}\n\nPrice : ${this.itemChoice.price} Gold` +
							`\n\nIf you want to buy it type merchant.buy('${this.itemChoice.code}')`;
						}
						
					} else {
						return `\nThis is not a valid choice !\n`;
					}
				}
			}
		} else {
			return notAllowed;
		}
	},

	buy(choice) {
		choice = choice[0].toLowerCase() + choice.substr(1);
		if (town.merchantPos > 0) {
			for (let o in items) {
				if (o == choice) {
					if (items[o].buyable) {
						if (hero.gold >= items[o].price) {
							hero.gold -= items[o].price;
							hero.compFunc = true;
							hero.addInv(items[o]);
							return `\nYou get ${items[o].name}\n\nThanks for your purchase !\n`
						} else {
							town.merchantPos = 0;
							town.position = 1;
							return `\nYou don't have enough gold ! \n\nYou've been kicked out by the shop owner and you're back in town.\n\n\n` +
							townDirections;
						}
					} 
				}
			}
			return `You can't buy that !`;
		} else {
			return notAllowed;
		}
	},

	sell(item = 'no') {
		itemToSell = false;
		if (town.merchantPos === 1) {
			if (item === 'no') {
				console.log(`\nType the command to sell the item.\n`);
				for (let o in hero.inventory) {
					let price = Math.floor(hero.inventory[o].price / 2);
					itemToSell = true;
					console.log(`\n${hero.inventory[o].name}  -  ${price} Gold  -  Possessed : ${hero.inventory[o].possessed}  -  merchant.sell('${o}')\n`);
				}
				if (!itemToSell) {
					return `\nYou have no item to sell.\n`;
				} else {
					return `\nEnd\n`;
				}
			} else {
				for (let o in hero.inventory) {
					if (o === item) {
						let price = Math.floor(hero.inventory[o].price / 2);
						hero.gold += price;
						if (hero.inventory[o].possessed > 1) {
							hero.inventory[o].possessed--;
						} else {
							delete hero.inventory[o];
						}
						return `\nYou sold ${items[o].name} for ${price}\n`;
					} 
				}
				return `\nIt's not a valid choice !\n`;
			}
		} else {
			return notAllowed;
		}
	}
};