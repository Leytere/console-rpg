class Town {
	constructor() {
		this.position = 0,
		this.innPos = 0,
		this.merchantPos = 0,
		this.spellsPos = 0,
		this.trainerPos = 0,
		this.guildPos = 0,
		this.dungeonPos = 0,
		this.innCost = 0,
		this.compFunc = false;
	}

	go(place) {
		if (this.position === 1) {
			place = place[0].toUpperCase() + place.substr(1);
			switch (place) {
				case 'Inn':
				inn.compFunc = true;
				this.innCost = inn.calcInnCost();
				this.innPos++;
				this.position--;
				return `\nWelcome to the inn, staying here will cost you ${this.innCost} Gold\n\n` +
				`You have ${hero.gold} Gold, do you want to stay ?\n\nType inn.stay() to stay.\n\nType inn.leave() to leave.\n`;
				break;

				case 'Merchant':
				this.position--;
				this.merchantPos++;
				return `\nWelcome to my shop, in what kind of item are you interested ?\n\n` + 
				`I have Weapons, Helmets, Chests, Boots, OffHands and Items.\n\n` +
				`You can also sell items you don't use anymore to me.\n\n\n` +
				`Type merchant.choice('Type') to browse through my wares and merchant.sell() to sell something to me.\n\n` +
				`Type merchant.leave() at anytime to return to town.\n`;
				break;

				case 'Spells':
				this.position--;
				this.spellsPos++;
				return `\nWelcome to the Spell Shop, we have Mage and Cleric Spells.\n\n\nType spells.type('mage') to see ` +
				`mage spells.\n\nType spells.type('cleric') to see cleric spells.\n\nType spells.leave() at anytime to return to town.\n`;
				break;

				case 'Trainer':
				this.position--;
				this.trainerPos++;
				return `\nWelcome to the Training Grounds.\n\nHere you can train your stats to be better in exchange of a price.\n\n` +
				`The price goes up everytime you train a stat so choose carefully !\n\n\n` +
				`Type trainer.stat('attack'), trainer.stat('defense'), trainer.stat('intelligence') or \ntrainer.stat('speed') to see ` +
				`the price and start the training.\n\nYou can leave at anytime with trainer.leave().\n`;
				break;

				case 'Guild':
				this.position--;
				this.guildPos++;
				return `\nWelcome to the Guild.\n\nHere you can accept quests to do in dungeons.\n\n` +
				`You can choose a dungeon to browse available quests by typing guild.dungeon('dungeon')\n\n` +
				`Dungeon List : \n\nForest\n\n\nYou can check if you need to complete a quest by typing guild.complete().` +
				`\n\nYou can leave anytime by typing guild.leave()\n`;
				break;

				case 'Dungeons':
				this.position--;
				this.dungeonPos++;
				return `\nYou look at your map to determine where you are going.\n\nYou can choose a dungeon ` +
				`by typing town.dungeon('DungeonName').\n\n\nDungeons List :\n\nForest\n\n\nType town.leave() to get back in town.\n`;
				break;

				default:
				return `\nThis is not an allowed choice !\n`;
				break;
			}
		} else {
			return `\nThis is not an allowed choice !\n`;
		}
	}

	dungeon(choice) {
		if (this.dungeonPos === 1) {
			choice = choice[0].toLowerCase() + choice.substr(1);
			switch (choice) {
				case 'forest':
				let forest = new Forest();
				hero.dungeon = 'forest';
				this.dungeonPos++;
				return `\nYou're now in the Forest.\n\n\nType game.help(3) to get info on how to play in dungeons.\n`
				break;

				default:
				return `\nThis is not an allowed choice !\n`;
				break;
			}
		} else {
			return notAllowed;
		}
	}

	leave() {
		if (this.position !== 1 && this.dungeonPos === 1) {
			this.position = 1;
			this.dungeonPos = 0;
			return `\nYou're back in town.\n\n\n` + townDirections;
		} else {
			return notAllowed;
		}
	}
}



let inn = {

	compFunc: false,

	calcInnCost() {
		if (this.compFunc) {
			this.compFunc = false;
			switch (hero.lvl) {
				case 1:
				return 20;
				break;

				case 2:
				return 60;
				break;

				case 3:
				return 180;
				break;

				case 4:
				return 300;
				break;

				default:
				return hero.lvl * 100;
				break;
			}
		}
	},

	stay() {
		if (town.innPos !== 0) {
			if (hero.gold < town.innCost) {
				town.innPos--;
				town.position++;
				return `\nYou don't have enough gold get out of here !\n\nYou're in town again...\n\n\n` + townDirections;
			} else {
				if (hero.xp >= hero.xpToLvl) {
					hero.lvlUp();
				}
				hero.hp = hero.maxHp;
				hero.mp = hero.maxMp;
				hero.gold -= town.innCost;
				town.innPos--;
				town.position++;
				return `\nYou're refreshed and back in town again.\n\n\n` + townDirections;
			}
		} else {
			return notAllowed;
		}
	},

	leave() {
		if (town.innPos !== 0) {
			town.innPos = 0;
			town.position = 1;
			return `\nYou're back to town.\n\n\n` + townDirections;
		} else {
			return notAllowed;
		}
	}	
};



let town = new Town();
