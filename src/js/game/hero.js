class Hero {
	constructor(name, role) {
		this.name = name,
		this.role = role,
		this.lvl = 1,
		this.xp = 0,
		this.xpToLvl = 50,
		this.hpMax = this.setHp(),
		this.mpMax = this.setMp(),
		this.hp = this.hpMax,
		this.mp = this.mpMax,
		this.baseAtk = this.setAtk(),
		this.baseDef = this.setDef(),
		this.baseInt = this.setInt(),
		this.baseSpe = this.setSpe(),
		this.spells = {},
		this.gold = 0,
		this.questLog = {},
		this.inventory = {},
		this.equip = {
			head: false,
			chest: items.clotharmor,
			weapon: items.stick,
			offHand: false,
			foot: items.clothboots

		},
		this.stockAtk = 0,
		this.stockDef = 1,
		this.stockSpe = 0,
		this.stockInt = 0,
		this.compFunc = false,
		this.onEvent = false,
		this.state = 'normal',
		this.roleAllowed = this.setRoleAllowed()
	}

	setRoleAllowed() {
		switch (this.role) {
				case 'Warrior':
				return [1, 2, 3];
				break;

				case 'Cleric':
				return [1, 2, 4, 6];
				break;

				case 'Mage':
				return [1, 5, 6];
				break;
			}
	}

	setHp() {
		switch (this.role) {
			case 'Warrior':
			return 23;
			break;

			case 'Cleric':
			return 15;
			break;

			case 'Mage':
			return 10;
			break;
		}
	}

	setMp() {
		switch (this.role) {
			case 'Warrior':
			return 5;
			break;

			case 'Cleric':
			return 13;
			break;

			case 'Mage':
			return 23;
			break;
		}
	}

	setAtk() {
		switch (this.role) {
			case 'Warrior':
			return 7;
			break;

			case 'Cleric':
			return 3;
			break;

			case 'Mage':
			return 1;
			break;
		}
	}

	setDef() {
		switch (this.role) {
			case 'Warrior':
			return 5;
			break;

			case 'Cleric':
			return 3;
			break;

			case 'Mage':
			return 1;
			break;
		}
	}

	setInt() {
		switch (this.role) {
			case 'Warrior':
			return 1;
			break;

			case 'Cleric':
			return 3;
			break;

			case 'Mage':
			return 5;
			break;
		}
	}

	setSpe() {
		switch (this.role) {
			case 'Warrior':
			return 3;
			break;

			case 'Cleric':
			return 4;
			break;

			case 'Mage':
			return 6;
			break;
		}
	}

	updateStats() {
		const def = this.calcEquipDef(),
			  atkInt = this.calcEquipWeap(),
			  spe = this.calcEquipSpe();

		this.def = def + this.baseDef;
		this.atk = atkInt[0] + this.baseAtk;
		this.int = atkInt[1] + this.baseInt;
		this.spe = spe + this.baseSpe;
	}

	calcEquipDef() {
		let def = 0;
		if (!this.equip.head) {
		} else {
			def += this.equip.head.def;
		}
		if (!this.equip.chest) {
		} else {
			def += this.equip.chest.def;
		}
		if (!this.equip.foot) {
		} else {
			def += this.equip.foot.def;
		}
		if (!this.equip.offHand) {
		} else if (this.equip.offHand.offHandType === 'shield') {
			def += this.equip.offHand.def;
		}
		return def;
	}

	calcEquipWeap() {
		let int = 0,
			atk = 0;
		if (!this.equip.offHand) {
		} else if (this.equip.offHand.offHandType === 'book') {
			int += this.equip.offHand.int;
		}
		if (!this.equip.weapon) {
		} else if (this.equip.weapon.weaponType === 'sword' || this.equip.weapon.weaponType === 'stick') {
			atk += this.equip.weapon.atk;
		} else if (this.equip.weapon.weaponType === 'mace') {
			atk += this.equip.weapon.atk;
			int += this.equip.weapon.int;
		} else if (this.equip.weapon.weaponType === 'staff') {
			int += this.equip.weapon.int;
		}
		return [atk, int];
	}

	calcEquipSpe() {
		let spe = 0;
		if (!this.equip.foot) {
		} else {
			spe += this.equip.foot.spe;
		}
		return spe;
	}

	addInv(item) {
		if (this.compFunc) {
			this.compFunc = false;
			for (let o in this.inventory) {
				if (this.inventory[o].name == item.name) {
					this.inventory[o].possessed++;
					return `\n${item.name} added to inventory.\n`;
				}
			}
			hero.inventory[item.code] = Object.create(item);
			hero.inventory[item.code].possessed++;
			return `\n${item.name} added to inventory.\n`;
		} else {
			return notAllowed;
		}
	}

	lvlUp() {
		if (this.compFunc) {
			this.compFunc = false;
			this.xp -= this.xpToLvl;
			this.xpToLvl = Math.floor(1.9 * this.xpToLvl);
			switch (this.role) {

				case 'Warrior':
				this.baseDef += 2;
				this.baseAtk += 2;
				this.baseSpe += 1;
				console.log(`\nYou've gained a level !\n`);
				if (this.stockInt === 0) {
					console.log(`\n+11 Hp\n\n+2 Mp\n\n+2 Atk\n\n+2 Def\n\n+1 Spe\n`);
					this.stockInt++;
				} else {
					console.log(`\n+11 Hp\n\n+2 Mp\n\n+2 Atk\n\n+2 Def\n\n+1 Spe\n\n+1 Int\n`);
					this.baseInt++;
					this.stockInt = 0;
				}
				this.lvl++;
				this.hpMax += 11;
				this.mpMax += 2;
				hero.updateStats();
				return `\nEnd\n`;
				break;

				case 'Cleric':
				this.baseDef++;
				this.baseAtk++;
				console.log(`\nYou've gained a level !\n`);
				if (this.stockSpe === 0) {
					console.log(`\n+7 Hp\n\n+7 Mp\n\n+1 Atk\n\n+1 Def\n\n+1 Int\n`);
					this.stockSpe++;
				} else {
					console.log(`\n+7 Hp\n\n+7 Mp\n\n+1 Atk\n\n+1 Def\n\n+1 Spe\n\n+1 Int\n`);
					this.baseSpe++;
					this.stockSpe--;
				}
				this.baseInt++;
				this.lvl++;
				this.hpMax += 7;
				this.mpMax += 7;
				hero.updateStats();
				return `\nEnd\n`;
				break;

				case 'Mage':
				this.baseInt += 2;
				console.log(`\nYou've gained a level !\n`);
				if (this.stockAtk === 0 && this.stockDef === 0) {
					console.log(`\n+4 Hp\n\n+13 Mp\n\n+2 Spe\n\n+2 Int\n`);
				} else if (this.stockAtk === 0 && this.stockDef === 1) {
					console.log(`\n+4 Hp\n\n+13 Mp\n\n+1 Def\n\n+2 Spe\n\n+2 Int\n`);
				} else if (this.stockAtk === 1 && this.stockDef === 0) {
					console.log(`\n+4 Hp\n\n+13 Mp\n\n+1 Atk\n\n+2 Spe\n\n+2 Int\n`);
				} else {
					console.log(`\n+4 Hp\n\n+13 Mp\n\n+1 Atk\n\n+1 Def\n\n+2 Spe\n\n+2 Int\n`);
				}
				if (this.stockAtk === 0) {
					this.stockAtk++;
				} else {
					this.baseAtk++;
					this.stockAtk--;
				}
				if (this.stockDef === 0) {
					this.stockDef++;
				} else {
					this.stockDef--;
					this.baseDef++;
				}
				this.baseSpe += 2;
				this.lvl++;
				this.hpMax += 4;
				this.mpMax += 13;
				hero.updateStats();
				return `\nEnd\n`;
				break;
			}
		} else {
			return notAllowed;
		}
	}

	status() {
		let state;
		console.log(`\n${this.name}\n\nClass : ${this.role}\n\nLvl : ${this.lvl}\n\n`);
		console.log(`${this.hp} / ${this.hpMax} Hp\n\n${this.mp} / ${this.mpMax} Mp\n\n`);
		console.log(`Attack : ${this.atk}\n\nDefense : ${this.def}\n\nIntelligence : ${this.int}\n\nSpeed : ${this.spe}\n\n\n`);
		console.log('Equipement : \n\n');


		if (!this.equip.head) {
			console.log(`Head : None\n\n`);
		} else {
			console.log(`Head : ${this.equip.head.name}  -  ${this.equip.head.def} Def\n\n`);
		}

		if (!this.equip.chest) {
			console.log('Chest : None\n\n');
		} else {
			console.log(`Chest : ${this.equip.chest.name}  -  ${this.equip.chest.def} Def\n\n`);
		}

		if (!this.equip.foot) {
			console.log('Foots : None\n\n');
		} else {
			console.log(`Foots : ${this.equip.foot.name}  -  ${this.equip.foot.def} Def  -  ${this.equip.foot.spe} Spe\n\n`);
		}


		if (!this.equip.weapon) {
			console.log('Weapon : None\n\n');

		} else if (this.equip.weapon.weaponType === 'sword') {
			console.log(`Weapon : ${this.equip.weapon.name}  -  ${this.equip.weapon.atk} Atk\n\n`);

		} else if (this.equip.weapon.weaponType === 'mace') {
			console.log(`Weapon : ${this.equip.weapon.name}  -  ${this.equip.weapon.atk} Atk - ` +
				`${this.equip.weapon.int} Int\n\n`);

		} else if (this.equip.weapon.weaponType === 'stick') {
			console.log(`Weapon : ${this.equip.weapon.name}  -  ${this.equip.weapon.atk} Atk\n\n`) 

		} else {
			console.log(`Weapon : ${this.equip.weapon.name}  -  ${this.equip.weapon.int} Int\n\n`)
		}


		if (!this.equip.offHand) {
			console.log(`OffHand : None\n\n`);

		} else if (this.equip.offHand.offHandType === 'shield') {
			console.log(`${this.equip.offHand.name}  -  ${this.equip.offHand.def} Def\n\n`);

		} else {
			console.log(`${this.equip.offHand.name}  -  ${this.equip.offHand.int} Int\n\n`);
		}
		state = this.state[0].toUpperCase() + this.state.substr(1);
		console.log(`\nState : ${state}`);
		return `\nEnd\n`;
	}

	bag() {
		let emptyInv = true;
		console.log(`\nYou have ${hero.gold} Gold\n`);
		for (let o in hero.inventory) {
			emptyInv = false;
			console.log(`\n${hero.inventory[o].name}  -  Possessed : ${hero.inventory[o].possessed}\n\n`);
		}
		if (emptyInv) {
			return `\nYour inventory is empty.\n`;
		} else {
			return `\nEnd\n`;
		}
	}

	spellList() {
		let emptySpells = true;
		for (let o in hero.spells) {
			emptySpells = false;
			console.log(`\n${hero.spells[o].name}  -  Cost ${hero.spells[o].mpCost} Mana\n`);
		}
		if (emptySpells) {
			return `\nYou have no spells.\n`;
		} else {
			return `\nEnd\n`;
		}
	}

	quests() {
		let emptyQuests = true;
		for (let o in hero.questLog) {
			emptyQuests = false;
			console.log(`\n${hero.questLog[o].name}  -  Reward : ${hero.questLog[o].reward} Gold\n\n` +
				`${hero.questLog[o].description}\n`);
		}
		if (emptyQuests) {
			return `\nYou have no quest.\n`;
		} else {
			return `\nEnd\n`;
		}
	}

	equipChange(choice = 'no') {
		if (!this.onCombat) {
			choice = choice[0].toLowerCase() + choice.substr(1);
			let noEquip = true;
			switch (choice) {
				case 'head':
				if (!hero.equip.head) {
					console.log(`\nYou have no helmet equiped.\n`);
				} else {
					console.log(`\nYour Helmet : ${hero.equip.head.name}  -  ${hero.equip.head.def} Def\n`);
					console.log(`\nUnequip  -  hero.unequip('helmet')\n`);
				}
				for (let o in hero.inventory) {
					if (hero.inventory[o].type === 'armor' && hero.inventory[o].armorType === 'head' && this.roleAllowed.includes(hero.inventory[o].roleAllowed)) {
						noEquip = false;
						console.log(`\n${hero.inventory[o].name}  -  ${hero.inventory[o].def} Def  -  ` +
							`hero.wear('${hero.inventory[o].code}')\n`);
					}
				}
				if (noEquip) {
					console.log(`\nYou don't have any helmet to equip.\n`);
				}
				return `\nYou can change your equipement by typing the given command.\n`;
				break;

				case 'chest':
				if (!hero.equip.chest) {
					console.log(`\nYou have no chest equiped.\n`);
				} else {
					console.log(`\nYour Chest : ${hero.equip.chest.name}  -  ${hero.equip.chest.def} Def\n`);
					console.log(`\nUnequip  -  hero.unequip('chest')\n`);
				}
				for (let o in hero.inventory) {
					if (hero.inventory[o].type === 'armor' && hero.inventory[o].armorType === 'chest' && this.roleAllowed.includes(hero.inventory[o].roleAllowed)) {
						noEquip = false;
						console.log(`\n${hero.inventory[o].name}  -  ${hero.inventory[o].def} Def  -  ` +
							`hero.wear('${hero.inventory[o].code}')\n`);
					}
				}
				if (noEquip) {
					console.log(`\nYou don't have any chest to equip.\n`);
				}
				return `\nYou can change your equipement by typing the given command.\n`;
				break;

				case 'weapon':
				if (!hero.equip.weapon) {
					console.log(`\nYou have no weapon equiped.\n`);
				} else if (hero.equip.weapon.weaponType === 'sword' || hero.equip.weapon.weaponType === 'stick') {
					console.log(`\nYour Weapon : ${hero.equip.weapon.name}  -  ${hero.equip.weapon.atk} Atk\n`);
					console.log(`\nUnequip  -  hero.unequip('weapon')\n`);
				} else if (hero.equip.weapon.weaponType === 'staff') {
					console.log(`\nYour Weapon : ${hero.equip.weapon.name}  -  ${hero.equip.weapon.int} Int\n`);
					console.log(`\nUnequip  -  hero.unequip('weapon')\n`);
				} else {
					console.log(`\nYour Weapon : ${hero.equip.weapon.name}  -  ${hero.equip.weapon.atk} Atk  -  ` +
						`${hero.equip.weapon.int} Int\n`);
					console.log(`\nUnequip  -  hero.unequip('weapon')\n`);
				}
				for (let o in hero.inventory) {
					if (hero.inventory[o].type === 'weapon' && this.roleAllowed.includes(hero.inventory[o].roleAllowed)) {
						noEquip = false;
						console.log(`\n${hero.inventory[o].name}  -  ${hero.inventory[o].def} Def  -  ` +
							`hero.wear('${hero.inventory[o].code}')\n`);
					}
				}
				if (noEquip) {
					console.log(`\nYou don't have any weapon to equip.\n`);
				}
				return `\nYou can change your equipement by typing the given command.\n`;
				break;

				case 'offHand':
				if (!hero.equip.offHand) {
					console.log(`\nYou have no off-hand equiped.\n`);
				} else if (hero.equip.offHand.offHandType === 'shield') {
					console.log(`\nYour Off-Hand : ${hero.equip.offHand.name}  -  ${hero.equip.offHand.def} Def\n`);
					console.log(`\nUnequip  -  hero.unequip('offHand')\n`);
				} else {
					console.log(`\nYour Off-Hand : ${hero.equip.offHand.name}  -  ${hero.equip.offHand.int} Int\n`);
					console.log(`\nUnequip  -  hero.unequip('offHand')\n`);
				}
				for (let o in hero.inventory) {
					if (hero.inventory[o].type === 'offHand' && this.roleAllowed.includes(hero.inventory[o].roleAllowed)) {
						noEquip = false;
						if (hero.inventory[o].offHandType === 'shield') {
							console.log(`\n${hero.inventory[o].name}  -  ${hero.inventory[o].def} Def  -  hero.wear('${hero.inventory[o].code}')\n`);
						} else {
							console.log(`\n${hero.inventory[o].name}  -  ${hero.inventory[o].int} Int  -  hero.wear('${hero.inventory[o].code}')\n`);
						}
					}
				}
				if (noEquip) {
					console.log(`\nYou don't have any off-hand to equip.\n`);
				}
				return `\nYou can change your equipement by typing the given command.\n`;
				break;

				case 'foot':
				if (!hero.equip.foot) {
					console.log(`\nYou have no boots equiped.\n`);
				} else {
					console.log(`\nYour Boots : ${hero.equip.foot.name}  -  ${hero.equip.foot.def} Def  -  ${hero.equip.foot.spe} Spe\n`);
					console.log(`\nUnequip  -  hero.unequip('foot')\n`);
				}
				for (let o in hero.inventory) {
					if (hero.inventory[o].type === 'armor' && hero.inventory[o].armorType === 'foot' && this.roleAllowed.includes(hero.inventory[o].roleAllowed)) {
						noEquip = false;
						console.log(`\n${hero.inventory[o].name}  -  ${hero.inventory[o].def} Def  -  ${hero.inventory[o].spe} Spe` +
							`  -  hero.wear('${hero.inventory[o].code}')\n`);
					}
				}
				if (noEquip) {
					console.log(`\nYou don't have any boots to equip.\n`);
				}
				return `\nYou can change your equipement by typing the given command.\n`;
				break;

				default:
				return `Choose which equipement you want to change : \n\n` +
				`Type hero.equipChange('command') to choose.\n\n\nList of commands :\n\n` +
				`Helmet : 'head'\n\nChest: 'chest'\n\nWeapon : 'weapon'\n\n` +
				`Off Hand : 'offHand'\n\nBoots : 'foot'\n`;
				break;
			}
		} else {
			return `\nYou're not allowed to do that while you fight !\n`;
		}
	}

	unequip(choice) {
		if (!this.onCombat) {
			choice = choice[0].toLowerCase() + choice.substr(1);
			switch (choice) {
				case 'helmet':
				if (!hero.equip.head) {
					return `\nYou don't have an helmet !\n`;
				} else {
					if (!hero.inventory[hero.equip.head.code]) {
						hero.inventory[hero.equip.head.code] = Object.create(hero.equip.head);
					} 
					hero.inventory[hero.equip.head.code].possessed++;
					hero.equip.head = false;
					hero.updateStats();
					return `\nYour helmet has been removed.\n`;
				}
				break;

				case 'chest':
				if (!hero.equip.chest) {
					return `\nYou don't have a chest !\n`;
				} else {
					if (!hero.inventory[hero.equip.chest.code]) {
						hero.inventory[hero.equip.chest.code] = Object.create(hero.equip.chest);
					} 
					hero.inventory[hero.equip.chest.code].possessed++;
					hero.equip.chest = false;
					hero.updateStats();
					return `\nYour chest has been removed.\n`;
				}
				break;

				case 'foot':
				if (!hero.equip.foot) {
					return `\nYou don't have boots !\n`;
				} else {
					if (!hero.inventory[hero.equip.foot.code]) {
						hero.inventory[hero.equip.foot.code] = Object.create(hero.equip.foot);
					} 
					hero.inventory[hero.equip.foot.code].possessed++;
					hero.equip.foot = false;
					hero.updateStats();
					return `\nYour boots has been removed.\n`;
				}
				break;

				case 'weapon':
				if (!hero.equip.weapon) {
					return `\nYou don't have a weapon !\n`;
				} else {
					if (!hero.inventory[hero.equip.weapon.code]) {
						hero.inventory[hero.equip.weapon.code] =Object.create(hero.equip.weapon);
					} 
					hero.inventory[hero.equip.weapon.code].possessed++;
					hero.equip.weapon = false;
					hero.updateStats();
					return `\nYour weapon has been removed.\n`;
				}
				break;

				case 'offHand':
				if (!hero.equip.offHand) {
					return `\nYou don't have an off-hand !\n`;
				} else {
					if (!hero.inventory[hero.equip.offHand.code]) {
						hero.inventory[hero.equip.offHand.code] = Object.create(hero.equip.offHand);
					} 
					hero.inventory[hero.equip.offHand.code].possessed++;
					hero.equip.offHand = false;
					hero.updateStats();
					return `\nYour off-hand has been removed.\n`;
				}
				break;

				default:
				return `\nIt's not a valid choice !\n`;
				break;
			}
		} else {
			return `\nYou can't do this while you fight !\n`;
		}
	}

	wear(choice) {
		if (!this.onCombat) {
			let wrongChoice = true,
			equipChoice;
			for (let o in hero.inventory) {
				if (choice == o && this.roleAllowed.includes(hero.inventory[o].roleAllowed)) {

					if (hero.inventory[o].type === 'weapon') {
						wrongChoice = false;
						equipChoice = hero.inventory[o];
						if (!hero.equip.weapon) {
							hero.equip.weapon = Object.create(equipChoice);
							hero.updateStats();
							if (hero.inventory[hero.equip.weapon.code].possessed > 1) {
								hero.inventory[hero.equip.weapon.code].possessed--;
							} else {
								delete hero.inventory[hero.equip.weapon.code];
							}
						} else {
							if (!hero.inventory[hero.equip.weapon.code]) {
								hero.inventory[hero.equip.weapon.code] = Object.create(hero.equip.weapon);
								hero.inventory[hero.equip.weapon.code].possessed = 1;
							} else {
								hero.inventory[hero.equip.weapon.code].possessed++; 
							}
							hero.equip.weapon = Object.create(equipChoice);
							hero.updateStats();
						}
					}

					if (hero.inventory[o].type === 'offHand') {
						wrongChoice = false;
						equipChoice = hero.inventory[o];
						if (!hero.equip.offHand) {
							hero.equip.offHand = Object.create(equipChoice);
							hero.updateStats();
							if (hero.inventory[hero.equip.offHand.code].possessed > 1) {
								hero.inventory[hero.equip.offHand.code].possessed--;
							} else {
								delete hero.inventory[hero.equip.offHand.code];
							}
						} else {
							if (!hero.inventory[hero.equip.offHand.code]) {
								hero.inventory[hero.equip.offHand.code] = Object.create(hero.equip.offHand);
								hero.inventory[hero.equip.offHand.code].possessed = 1;
							} else {
								hero.inventory[hero.equip.offHand.code].possessed++; 
							}
							hero.equip.offHand = Object.create(equipChoice);
							hero.updateStats();
						}
					}

					if (hero.inventory[o].type === 'armor') {

						if (hero.inventory[o].armorType === 'chest') {
							wrongChoice = false;
							equipChoice = hero.inventory[o];
							if (!hero.equip.chest) {
								hero.equip.chest = Object.create(equipChoice);
								hero.updateStats();
								if (hero.inventory[hero.equip.chest.code].possessed > 1) {
									hero.inventory[hero.equip.chest.code].possessed--;
								} else {
									delete hero.inventory[hero.equip.chest.code];
								}
							} else {
								if (!hero.inventory[hero.equip.chest.code]) {
									hero.inventory[hero.equip.chest.code] = Object.create(hero.equip.chest);
									hero.inventory[hero.equip.chest.code].possessed = 1;
								} else {
									hero.inventory[hero.equip.chest.code].possessed++; 
								}
								hero.equip.chest = Object.create(equipChoice);
								hero.updateStats();
							}

						} else if (hero.inventory[o].armorType === 'foot') {
							wrongChoice = false;
							equipChoice = hero.inventory[o];
							if (!hero.equip.foot) {
								hero.equip.foot = Object.create(equipChoice);
								hero.updateStats();
								if (hero.inventory[hero.equip.foot.code].possessed > 1) {
									hero.inventory[hero.equip.foot.code].possessed--;
								} else {
									delete hero.inventory[hero.equip.foot.code];
								}
							} else {
								if (!hero.inventory[hero.equip.foot.code]) {
									hero.inventory[hero.equip.foot.code] = Object.create(hero.equip.foot);
									hero.inventory[hero.equip.foot.code].possessed = 1;
								} else {
									hero.inventory[hero.equip.foot.code].possessed++; 
								}
								hero.equip.foot = Object.create(equipChoice);
								hero.updateStats();
							}

						} else if (hero.inventory[o].armorType === 'head') {
							wrongChoice = false;
							equipChoice = hero.inventory[o];
							if (!hero.equip.head) {
								hero.equip.head = Object.create(equipChoice);
								hero.updateStats();
								if (hero.inventory[hero.equip.head.code].possessed > 1) {
									hero.inventory[hero.equip.head.code].possessed--;
								} else {
									delete hero.inventory[hero.equip.head.code];
								}
							} else {
								if (!hero.inventory[hero.equip.head.code]) {
									hero.inventory[hero.equip.head.code] = Object.create(hero.equip.head);
									hero.inventory[hero.equip.head.code].possessed = 1;
								} else {
									hero.inventory[hero.equip.head.code].possessed++; 
								}
								hero.equip.head = Object.create(equipChoice);
								hero.updateStats();
							}
						}

					}

				}
			}
		} else {
			return notAllowed
		}
	}

	where() {
		if (town.position === 1) {
			return `\nYou're in Town.\n\n\n` + townDirections;
		} else if (town.merchantPos > 0) {
			return `\nYou're at the Shop.\n`;
		} else if (town.innPos > 0) {
			return `\nYou're at the Inn.\n`;
		} else if (town.spellsPos > 0) {
			return `\nYou're at the Spell Shop.\n`;
		} else if (town.trainerPos > 0) {
			return `\nYou're at the Training Grounds.\n`;
		} else if (town.guildPos > 0) {
			return `\nYou're at the Adventurers Guild.\n`;
		} else if (town.dungeonPos > 0) {
			if (forest.on) {
				return `\nYou're in the Forest.\n\nIt's been ${forest.heroPos} hours since you started this dungeon.\n`;
			}
		}
	}

	battle() {
		if (this.onCombat) {

			if (this.compFunc) {
				console.log(`\nYou face :\n`);
				this.compFunc = false;
				for (let i = 0; i < game.battleConf.enemies.length; i++) {
					console.log(`\n${game.battleConf.enemies[i].name}\n`);
				}
				console.log(`\n${hero.name}  -  ${hero.hp} / ${hero.hpMax} Hp  -  ${hero.mp} / ${hero.mpMax} Mp\n`);
				console.log(`\nIf you want info on enemies to attack type hero.attack()\n` +
					`\nIf you want to defend yourself, type hero.defend()\n` +
					`\nIf you want to use a skill type hero.skill('skillname')\n` +
					`\nIf you want infos on your available skills, type hero.skills()\n` +
					`\nIf you want to try to escape, type hero.run().\n`);

			} else {
				return notAllowed;
			}
		} else {
			return notAllowed;
		}
	}

	attack(choice = 'no') {
		if (this.onCombat) {
			for (let i = 0; i < game.battleConf.enemies.length; i++) {
				if (game.battleConf.enemies[i].codeC == choice) {
					if (game.calcHit(hero)) {

						let atkVal = hero.atk - game.battleConf.enemies[i].def;
						if (game.battleConf.enemies[i].defend) {
							atkVal = Math.floor(atkVal / 2);
						}
						if (atkVal <= 0) {
							console.log(`\nYou attack ${game.battleConf.enemies[i].name}\n\n` +
								`Your attack did 0 damages...\n`);

						} else {
							game.battleConf.enemies[i].hp -= atkVal;
							console.log(`\nYou attack ${game.battleConf.enemies[i].name}. Your ` +
								`attack did ${atkVal} damages.\n\n`);
							if (game.battleConf.enemies[i].hp <= 0) {
								console.log(`\nYou defeated ${game.battleConf.enemies[i].name} !\n`);
								game.battleConf.enemies[i] = false;
								game.battleConf.turn++;
								game.compFunc = true;
								game.battleTurn();
								return `\nEnd\n`;

							} else {
								game.battleConf.turn++;
								game.compFunc = true;
								game.battleTurn();
								return `\nEnd\n`;
							}
						}

					} else {

						console.log(`\nYou try to hit ${game.battleConf.enemies[i].name} but missed !\n`);
						game.battleConf.turn++;
						game.compFunc = true;
						game.battleTurn();
						return `\nEnd\n`;
					}

				}
			}
			if (choice === 'no') {
					console.log(`\nType the command next to the enemy name to attack it\n`);
					for (let i = 0; i < game.battleConf.enemies.length; i++) {
						console.log(`\n${game.battleConf.enemies[i].name}  -  hero.attack('${game.battleConf.enemies[i].codeC}')\n`);
					}
			} else {
				return `\nIt's not an allowed choice !\n`;
			}

		} else {
			return notAllowed;
		}
	}

	defend() {
		if (this.onCombat) {

			this.onDefense = true;
			game.battleConf.turn++;
			game.compFunc = true;
			console.log(`\nYou defend yourself for the new turn.\n`);
			game.battleTurn();
			return `\nEnd\n`;

		} else {
			return notAllowed;
		}
	}
}