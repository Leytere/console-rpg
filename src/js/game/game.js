class Game  {
	constructor() {
		this.position = 0,
		this.role = null,
		this.compFunc = false,
		this.battleConf = {
			initEnemies: [],
			enemies: [],
			tempChoice: [],
			dungeon: null
		}
	}
	
	help(num) {
		switch (num) {

			case 1:
			return townDirections;
			break;

			case 2:
			return `\nType hero.status() to get your stats.\n\nType hero.bag() to see your items.\n\n` +
			`Type hero.spellList() to see the list of your spells.\n\nType hero.quests() to see the list of your active quests.` +
			`\n\nType hero.equipChange() to get help for changing equipement.\n\nType hero.where() to see where you are.\n`;
			break;

			default:
			return '\nWhat do you need to know ?\n\n\n1 - Town\n\n2 - Hero\n\n\nType game.help(number)';
			break;
		}
	}

	start() {
		if (this.position === 0) {
			this.position = 1;
			return `\nWhat class do you want ?\n\nWarrior\n\nCleric\n\nMage\n\n\nType game.class('Your class')\n`;
		} else {
			return notAllowed;
		}
	}

	class(choice) {
		if (this.position === 1) {
			choice = choice[0].toUpperCase() + choice.substr(1);
			switch (choice) {

				case 'Warrior':
				this.role = 'Warrior';
				this.position = 2;
				return `\nWhat's your name ?\n\n\nType game.name('Your name')\n`;
				break;

				case 'Cleric':
				this.role = 'Cleric';
				this.position = 2;
				return `\nWhat's your name ?\n\n\nType game.name('Your name')\n`;
				break;

				case 'Mage':
				this.role = 'Mage';
				this.position = 2;
				return `\nWhat's your name ?\n\n\nType game.name('Your name')\n`;
				break;

				default:
				return `\nIt's not an allowed choice !\n`;
				break;
			}
		} else {
			return notAllowed;
		}
	}

	name(nameC) {
		if (this.position === 2) {
			if (typeof nameC === 'string') {
				nameC = nameC[0].toUpperCase() + nameC.substr(1);
				hero = new Hero(nameC, this.role);
				hero.updateStats();
				this.position = 3;
				town.position = 1;
				return `\nYou just woke up in Mark Town's inn, you travelled here two days ago looking for fame and glory.\n\n\n` + 
				townDirections;
			} else {
				return notAllowed;
			}
		} else {
			return notAllowed;
		}
	}

	eventLaunch(choice) {
		if (this.compFunc) {
			switch (choice.type) {

				case 'battle':
				game.battleInit(choice.monsters);
				break;

				case 'trap':
				game.trap(choice);
				break;

				case 'restoration':
				game.restoration(choice);
				break;
			}
		} else {
			return notAllowed;
		}
	}

	restoration(choice) {
		if (this.compFunc) {
			this.compFunc = false;
			let weakHp = _.random(10, 18),
				weakMp = _.random(6, 12);
			switch (choice.restoType) {

				case 'weakHp':
				hero.hp += weakHp;
				if (hero.hp > hero.hpMax) {
					hero.hp = hero.hpMax;
				}
				console.log(`\nYou've gained ${weakHp} Hp !\n`);
				break;

				case 'weakMp':
				hero.mp += weakMp;
				if (hero.mp > hero.mpMax) {
					hero.mp = hero.mpMax;
				}
				console.log(`\nYou've gained ${weakMp} Mp !\n`);
				break;
			}
		} else {
			return notAllowed;
		}
	}

	trap(choice) {
		if (this.compFunc) {
			this.compFunc = false;
			let weakHp = _.random(6, 12),
				weakMp = _.random(3, 7);
			switch (choice.trapType) {

				case 'weakHp':
				hero.hp -= weakHp;
				if (hero.hp <= 0) {
					this.compFunc = true;
					hero.dungeon = null;
					switch (hero.dungeon) {
						case 'forest':
						forest.delete();
						break;
					}
					this.death();
				}
				console.log(`\nYou've lost ${weakHp} Hp !\n`);
				break;

				case 'weakMp':
				hero.mp -= weakMp;
				if (hero.mp < 0) {
					hero.mp = 0;
				}
				console.log(`\nYou've lost ${weakMp} Mp !\n`);
				break;
			}
		} else {
			return notAllowed;
		}
	}

	battleInit(monsters) {
		if (this.compFunc) {

			this.battleConf.initEnemies = Array.from(monsters);
			this.battleConf.enemies = Array.from(monsters);
			hero.onCombat = true;
			for (let i = 0; i < this.battleConf.enemies.length; i++) {
				this.battleConf.enemies[i].codeC = '';
				this.battleConf.enemies[i].codeC += this.battleConf.enemies[i].code;
			}
			for (let i = 0; i < this.battleConf.enemies.length; i++) {
				let verif = false;
				for (let j = 0; j < this.battleConf.enemies.length; j++) {
					if (i == j) {

					} else if (this.battleConf.enemies[i].name === this.battleConf.enemies[j].name) {
						verif = true;
					}
				}
				if (verif) {
					this.battleConf.enemies[i].codeC += i.toString();
				}
			}
			console.log(`\nYou face ${this.battleConf.enemies.length} monsters !\n`);
			for (let i = 0; i < this.battleConf.enemies.length; i++) {
				console.log(`\n${this.battleConf.enemies[i].name} appear !\n`);
			}
			this.battleTurnO();

		} else {
			return notAllowed;
		}
	}

	battleTurnO() {
		if (this.compFunc) {
			
			this.battleConf.tempChoice = Array.from(this.battleConf.enemies);
			this.battleConf.turnOrder = [];
			this.battleConf.bestSpeed = 0;
			this.battleConf.bestSpeedO = null;
			this.battleConf.speedIndex = 0;
			this.battleConf.tempChoice.push(hero);
			
			while (this.battleConf.tempChoice.length !== 0) {
				for (let i = 0; i < this.battleConf.tempChoice.length; i++) {
					if (this.battleConf.tempChoice[i].spe > this.battleConf.bestSpeed) {

						this.battleConf.bestSpeed = this.battleConf.tempChoice[i].spe;
						this.battleConf.bestSpeedO = this.battleConf.tempChoice[i];
						this.battleConf.speedIndex = i;
					}
				}
				this.battleConf.turnOrder.push(this.battleConf.bestSpeedO);
				this.battleConf.tempChoice.splice(this.battleConf.speedIndex, 1);
				this.battleConf.bestSpeed = 0;
			}

			this.battleConf.turn = 0;
			this.battleTurn();

		} else {
			return notAllowed;
		}
	}

	st() {
		town.position = 1;
		hero = new Hero('ley', 'Warrior');
		hero.updateStats();
		this.position = 3;
	}

	battleTurn() {
		if (this.compFunc) {

			if (this.battleConf.turnOrder[this.battleConf.turn]) {

				if (this.battleConf.turnOrder[this.battleConf.turn] instanceof Hero) {
					this.compFunc = false;
					hero.compFunc = true;
					hero.onDefense = false;
					hero.battle();
				} else {
					this.battleConf.monsterPending = this.battleConf.turnOrder[this.battleConf.turn];
					this.monsterTurn();
				}

			} else {

				if (this.battleConf.turn >= this.battleConf.turnOrder.length) {

					if (this.battleConf.enemies.length <= 0) {
						this.battleSuccess();

					} else {
						this.battleTurnO();
					}
				} else {
					this.battleConf.turn++;
					this.battleTurn();
				}
			}

		} else {
			return notAllowed;
		}
	}

	monsterTurn() {
		if (this.compFunc) {

			this.battleConf.monsterPending.defend = false;
			this.battleConf.enemyChoice = _.random(1, 100);
			if (this.battleConf.monsterPending.skillChances) {

				for (let i = 0; i < this.battleConf.monsterPending.skillChances.length; i++) {

					if (i === 0) {

						if (this.battleConf.enemyChoice <= this.battleConf.monsterPending.skillChances[i][0]) {

							this.battleConf.enemySkillOn = true;
							this.battleConf.enemySkill = this.battleConf.monsterPending.skills[this.battleConf.monsterPending.skillChances[i][1]];
						}

					} else if (this.battleConf.enemyChoice <= this.battleConf.monsterPending.skillChances[i][0]
						&& this.battleConf.enemyChoice > this.battleConf.monsterPending.skillChances[i - 1][0]) {

						this.battleConf.enemySkillOn = true;
						this.battleConf.enemySkill = this.battleConf.monsterPending.skills[this.battleConf.monsterPending.skillChances[i][1]];
					}
				}

			}
			if (this.battleConf.enemySkillOn) {
				if (this.battleConf.enemySkill.type === 'defend') {
					this.battleConf.enemySkillOn = false;
					this.monsterDefend();
				} else {
					this.battleConf.enemySkillOn = false;
					this.monsterSkill();
				}
				
			} else {
				this.monsterAttack();
			}

		} else {
			return notAllowed;
		}
	}

	monsterDefend() {
		if (this.compFunc) {

			this.battleConf.monsterPending.defend = true;
			this.battleConf.turn++;
			console.log(`${this.battleConf.monsterPending.name} defend themself.\n`);
			this.battleTurn();
		}
	}

	monsterAttack() {
		if (this.compFunc) {

			if (this.calcHit(this.battleConf.monsterPending)) {

				this.battleConf.monAtkVal = this.battleConf.monsterPending.atk - hero.def;
				if (hero.onDefense) {
					this.battleConf.monAtkVal = Math.floor(this.battleConf.monAtkVal / 2);
				}
				if (this.battleConf.monAtkVal < 0) {
					this.battleConf.monAtkVal = 0;
					console.log(`\n${this.battleConf.monsterPending.name} Attack you !\n\nThe attack didn't do anything to you !\n`);
					this.battleConf.turn++;
					this.battleTurn();
				} else {
					hero.hp -= this.battleConf.monAtkVal;
					if (hero.hp < 0) {
						hero.hp = 0;
					}
					console.log(`\n${this.battleConf.monsterPending.name} attack you !\n\n` +
						`You lose ${this.battleConf.monAtkVal} Hp.\n\nYou have ${hero.hp} Hp left.\n`);
					if (hero.hp <= 0) {
						console.log(`\nYou have no Hp left, you're dead.\n`);
						this.death(this.battleConf.dungeon);
					}
					this.battleConf.turn++;
					this.battleTurn();
				}
			} else {
				console.log(`\n${this.battleConf.monsterPending.name} try to attack you, but miss.\n`);
				this.battleConf.turn++;
				this.battleTurn();
			}

		} else {
			return notAllowed;
		}
	}

	calcHit(unit) {
		if (this.compFunc) {
			let rand = _.random(1, 100);
			if (unit instanceof Hero) {
				if (rand <= unit.equip.weapon.hit) {
					return true;
				} else {
					return false;
				}
			} else {
				if (rand <= unit.hit) {
					return true;
				} else {
					return false;
				}
			}

		} else {
			return notAllowed;
		}
	}

	battleTest() {
		this.compFunc = true;
		this.battleInit([Object.create(monsters.slime)]);
	}

	death(dungeon = 'no') {
		if (this.compFunc) {
			if (dungeon !== 'no') {
				hero.dungeon = null;
				switch (hero.dungeon) {
					case 'forest':
					forest.delete();
					break;
				}
			}
			let xpLost = Math.floor(hero.xp / 5);
			let tempXp = hero.xp;
			this.compFunc = false;
			hero.xp -= xpLost
			if (hero.xp < 0) {
				hero.xp = 0;
				xpLost = tempXp;
			}
			hero.hp = hero.hpMax;
			hero.mp = hero.mpMax;
			console.log(`\nYou were dead and have been resurrected in exchange for 20% of your Xp Points.\n\n` +
			`You lost ${xpLost} Xp Points.\n\nYou're back in town...\n\n\n` + townDirections);
		} else {
			return notAllowed;
		}
	}
}

let game = new Game();